#include "main.h"

#include <Arduino.h>
#include <RF24.h>
#include <RF24Network.h>

#include "Servo.h"

// nRF24L01(+)
RF24 radio(7, 8);                  // CE, CSN
RF24Network network(radio);        // Network uses that radio
const uint16_t baseNode = 00;      // Adress of our base (Octal format)
const uint16_t launcherNode = 02;  // Address of the Launcher (Octal format)
uint8_t dataBuffer[MAX_PAYLOAD_SIZE];

Servo servo;

bool launcherLocked = true;

void setup() {
    servo.attach(12);
    servo.write(0);

    if (radio.begin() && radio.isChipConnected()) {
        radio.setChannel(90);
        radio.setPALevel(RF24_PA_MIN);
        radio.setDataRate(RF24_250KBPS);
        network.begin(launcherNode);
    }

    Serial.begin(9600);
}

void loop() {
    network.update();  // Check the network regularly

    while (network.available()) {
        RF24NetworkHeader header;
        uint16_t payloadSize = network.peek(header);
        network.read(header, &launcherLocked, payloadSize);
    }

    if (!launcherLocked) {
        unlockLauncher();
    }
}

void unlockLauncher() { servo.write(90); }